<?php


abstract class AbstractRestController  extends \Phalcon\Mvc\Controller
{
    /**
     * @var \Phalcon\Http\Response
     */
    protected $response;

    public function beforeExecuteRoute($dispatcher)
    {
        $this->response = $dispatcher->getDI()->getShared("response");
    }

    public function afterExecuteRoute($dispatcher)
    {
        $dispatcher->getDI()->getShared("response")->setContentType('application/json', 'UTF-8');
        $dispatcher->getDI()->getShared("view")->disable();
    }
} 