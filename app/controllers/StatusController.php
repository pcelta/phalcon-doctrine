<?php

/**
 * @RoutePrefix("/status")
 */
class StatusController extends AbstractRestController
{

    /**
     * @Route("/", methods={"GET"}, name="status.index")
     */
    public function indexAction()
    {
        $this->response->setStatusCode(200, "");
        $this->response->setContent(json_encode(["message"=>"Phalcon and Doctrine Skeleton it's working! =)"]));
        $this->response->send();
    }
}