<?php

namespace Base;

class ParameterManager
{

    /**
     * @var ParameterManger
     */
    private static $instance;

    private $parameters = [];

    private function __clone()
    {
    }
    private function __construct()
    {
        $this->loadParameters();
    }

    /**
     * @return ParameterManager
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function loadParameters()
    {
        $configPath = __DIR__ . '/../../../configs/parameters.php';
        if (!file_exists($configPath)) {
            throw new ParametersFileNotFoundException("configs/parameters.php NOT FOUND");
        }

        $this->parameters = require_once $configPath;
    }

    public function get($parameter)
    {
        if (!array_key_exists($parameter, $this->parameters)) {
            throw new \InvalidArgumentException(sprintf('Parameter <%s> NOT FOUND', $parameter));
        }

        return $this->parameters[$parameter];
    }
} 