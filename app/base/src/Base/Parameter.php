<?php

namespace Base;

class Parameter 
{

    public static function get($key)
    {
        return $parameterManager = ParameterManager::getInstance()->get($key);
    }

    public static function isDevelopment()
    {
        $isDevelopment = self::get("is_development");
        if (is_bool($isDevelopment)) {
            return $isDevelopment;
        }

        return false;
    }
} 