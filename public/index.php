<?php

$loader = new \Phalcon\Loader();
$di = \Phalcon\DI::getDefault();

try {

    $loader->registerDirs(array(
        '../app/controllers/'
    ))->register();

    require_once __DIR__ ."/../bootstrap.php";

    $di = \Phalcon\DI::getDefault();

    //Setup the view component
    $di->set('view', function(){
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir('../app/views/');
        return $view;
    });

    $di->set('url', function(){
        $url = new \Phalcon\Mvc\Url();
        $url->setBaseUri('/');
        return $url;
    });

/*  for rest controllers
    $di['router'] = function() {
        $router = new \Phalcon\Mvc\Router\Annotations(false);
        $router->addResource('Post', '/post');

        return $router;
    };*/

    //Handle the request
    $application = new \Phalcon\Mvc\Application($di);

    echo $application->handle()->getContent();

} catch (\Phalcon\Exception $e) {
    $response = $di->getShared("response");
    $response->setHeader("Content-Type", "application/json");
    $response->setStatusCode(503, "");$errorResponse = \Base\Parameter::get("generic_response_error");
    if (\Base\Parameter::isDevelopment()) {
        $errorResponse["trace"] = $e->getMessage();
    }
    $content = json_encode($errorResponse);
    $response->setContent($content);
    $response->send();
} catch (\Exception $e) {
    echo $e->getMessage();
    $response = $di->getShared("response");
    $response->setHeader("Content-Type", "application/json");
    $response->setStatusCode(503, "");
    $errorResponse = \Base\Parameter::get("generic_response_error");
    if (\Base\Parameter::isDevelopment()) {
        $errorResponse["trace"] = $e->getMessage();
    }
    $content = json_encode($errorResponse);
    $response->setContent($content);
    $response->send();
}