<?php

$composerLoader = require_once __DIR__.'/vendor/autoload.php';

$composerLoader instanceOf \Composer\Autoload\ClassLoader;
$composerLoader->add('Shared', __DIR__.'/followbands-shared/src/');
$composerLoader->add('Timeline', __DIR__.'/app/timeline/src/');
$composerLoader->add('Base', __DIR__.'/app/base/src/');

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Base\Parameter;
use Base\ServiceLocator;

$di = new Phalcon\DI\FactoryDefault();

$di->set("entityManager", function() {
    $dbParams = Parameter::get("db");
    $entitiesPaths = Parameter::get("entities_path");
    $proxiesPath = null;//Parameter::get("proxies_path");
    $isDevMode = true;
    $cache = new \Doctrine\Common\Cache\ArrayCache();
    $config = Setup::createAnnotationMetadataConfiguration($entitiesPaths, $isDevMode, $proxiesPath, $cache, false);

    return EntityManager::create($dbParams, $config);
});

$di->set("cacheManager", function() {
    $cacheParams = Parameter::get("cache");
    $mongoClient = new \MongoClient($cacheParams["host"], $cacheParams["options"]);

    return $mongoClient->selectDB($cacheParams["options"]["db"]);
});

