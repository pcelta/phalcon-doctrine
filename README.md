### Skeleton Phalcon and Doctrine

#### Setup

-

```sh
$ git clone git@github.com:pcelta/phalcon-doctrine.git
$ cd /path-to-project/
$ curl -sS https://getcomposer.org/installer | php
$ composer.phar install
$ cp /path-to-project/app/configs/parameters.php.dist /path-to-project/app/configs/parameters.php

```

Edit the database settings(database, user, password) in the file: /path-to-project/app/configs/parameters.php
-

```php
    "db" => [
        'driver'   => 'pdo_mysql',
        'user'     => 'root',
        'password' => 'root',
        'dbname'   => 'databasename',
    ]
```

Create VirtualHost targeting to /<path-to-project>/public/
 
Enable rewrite module (Apache case)
-

```sh
$ sudo a2enmod rewrite
$ sudo service apache2 restart
```

Após todas essas configurações é chamar por GET a URL de status

```sh
$ curl <your-host/status>
{"message":"Phalcon and Doctrine Skeleton it's working! =)"}
```
